BASE := maxs/
MODULES := $(shell find -mindepth 1 -maxdepth 1 -type d -name 'module-*')
MODULES_MAKEFILE := $(foreach mod, $(MODULES), $(mod)/Makefile)
CPUS := $(shell grep -c ^processor /proc/cpuinfo)
ALL := $(MODULES)

.PHONY: all submodules $(ALL)

clean:
	TARGET=$@ $(MAKE) $(ALL)

parclean:
	TARGET=clean $(MAKE) -j$(CPUS) $(ALL)

distclean:
	TARGET=$@ $(MAKE) $(ALL)
	[ -d .git ] && git clean -x -d -f

deploy:
	TARGET=$@ $(MAKE) $(ALL)

pardeploy:
	TARGET=deploy $(MAKE) -j$(CPUS) $(ALL)

eclipse:
	TARGET=$@ $(MAKE) $(ALL)

parallel:
	$(MAKE) -j$(CPUS)

makefiles: $(MODULES_MAKEFILE)

$(ALL): makefiles
	cd $@ && $(MAKE) $(TARGET)

submodules:
	git submodule init
	git submodule update

module-%/Makefile:
	 ln -rs $(BASE)/build/module-makefile $@
