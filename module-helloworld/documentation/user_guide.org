#+TITLE:        MAXS Module Bluetooth: User Guide
#+AUTHOR:       Florian Schmaus
#+EMAIL:        flo@geekplace.eu
#+OPTIONS:      author:nil
#+STARTUP:      noindent

* Overview

Query the default helloworld adapter for its status.

Uses =android.permission.BLUETOOTH= permission.

* Commands

** =helloworld= (short command: =bt=)

*** =helloworld status=

#+BEGIN_SRC
User: helloworld status
AndroidDevice: Bluetooth is enabled: true
#+END_SRC
