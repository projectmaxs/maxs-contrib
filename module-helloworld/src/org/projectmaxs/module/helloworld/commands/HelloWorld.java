package org.projectmaxs.module.helloworld.commands;

import org.projectmaxs.shared.global.Message;
import org.projectmaxs.shared.global.messagecontent.CommandHelp.ArgType;
import org.projectmaxs.shared.mainmodule.Command;
import org.projectmaxs.shared.module.MAXSModuleIntentService;
import org.projectmaxs.shared.module.SubCommand;
import org.projectmaxs.shared.module.SupraCommand;

public class HelloWorld extends SubCommand {

	public HelloWorld() {
		super(new SupraCommand("hello", "h"), "world");
		setHelp(ArgType.NONE, "Return the famous 'Hello World!' message");
	}

	@Override
	public Message execute(String arguments, Command command,
			MAXSModuleIntentService service) throws Throwable {
		return new Message("Hello World!");
	}

}
